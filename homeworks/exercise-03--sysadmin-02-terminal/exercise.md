
# Домашнее задание к занятию "3.2. Работа в терминале, лекция 2"


1. Какого типа команда cd? Попробуйте объяснить, почему она именно такого типа; опишите ход своих мыслей, если считаете что она могла бы быть другого типа.

```bash
$ type -a cd
cd - это встроенная команда bash
```
  - `cd`(change directory) — изменить каталог
  - смена текущего каталога в текущем shell окружение 
  - если `cd` был внешней программой, то при вызове происходила инициализация нового окружения и дополнительный дочерний процесс

2. Какая альтернатива без `pipe` команде `grep <some_string> <some_file> | wc -l`? `man grep` поможет в ответе на этот вопрос. Ознакомьтесь с документом о других подобных некорректных вариантах использования `pipe`.

  - Pipe перенаправляет стандартный вывод на стандартный ввод, но не как аргумент команды
  - Типы каналов в Linux: Безымянный и Именованные каналы
    - Безымянный: Они создаются на лету вашей оболочкой Unix всякий раз, когда вы используете символ `|`:
      ```bash
      $ grep -e root -e www-data /etc/group
      root:x:0:
      www-data:x:33:
      $ grep -e root -e www-data /etc/group | wc -l
      2
      ``` 
    - Именованны: Они существуют как обычный файл. Вы можете создать именованный файл: 
    ```
    terminal_1:
    
    $ mkfifo pipe.file
    $ echo test > c
    ## NOTE: терминал звис пока не будет прочтен вывод #
    
    terminal_1:
    $ cat pipe.file
    test
    ## NOTE: оба терминала отпустило #   
    ```

*UPDATE:*

- Какая альтернатива без `pipe`...
```bash
$ grep -e root -e www-data /etc/group -c
2
```

3. Какой процесс с PID 1 является родителем для всех процессов в вашей виртуальной машине Ubuntu 20.04?

```bash
$ pstree -p
systemd(1)─┬─ModemManager(1176)─┬─{ModemManager}(1218)
           │                    └─{ModemManager}(1221)
```

4. Как будет выглядеть команда, которая перенаправит вывод stderr ls на другую сессию терминала?

    - terminal 1:
        ```bash
        $ echo $$
        20774
        $ pgrep -a bash
        20774 bash
        63774 bash
        $ ls -l \root 2>/proc/63774/fd/1
        ```

    - terminal 2:
        ```bash
        $ echo $$
        63774
        $
        ls: невозможно получить доступ к 'root': Нет такого файла или каталога
        ```
5. Получится ли одновременно передать команде файл на stdin и вывести ее stdout в другой файл? Приведите работающий пример.

```bash
$ echo "test-123" > /tmp/test-1.txt
$ cat /tmp/test-1.txt
test-123
$ cat /tmp/test-2.txt
cat: /tmp/test-2.txt: Нет такого файла или каталога
$ cat </tmp/test-1.txt >/tmp/test-2.txt
$ cat /tmp/test-2.txt
test-123
```

6. Получится ли находясь в графическом режиме, вывести данные из PTY в какой-либо из эмуляторов TTY? Сможете ли вы наблюдать выводимые данные?

  - из консоли виртуальной машины VirtualBox --> терминальный ssh клиент
    ![vagrant-terminal](screenshot/vagrant-terminal.png?raw=true "vagrant-terminal")

7. Выполните команду bash 5>&1. К чему она приведет? Что будет, если вы выполните echo netology > /proc/$$/fd/5? Почему так происходит?

  - `bash 5>&1` - Создаст дескриптор с `5` и перенатправит его в `stdout` 
  - `echo test > /proc/$$/fd/5` - выведет в дескриптор `5`, который был пернеаправлен в `stdout`

```bash
$ echo test > /proc/$$/fd/5
bash: /proc/20774/fd/5: Нет такого файла или каталога
$ bash 5>&1
$ echo test > /proc/$$/fd/5
test
```

8. Получится ли в качестве входного потока для pipe использовать только stderr команды, не потеряв при этом отображение stdout на pty? Напоминаем: по умолчанию через pipe передается только stdout команды слева от | на stdin команды справа. Это можно сделать, поменяв стандартные потоки местами через промежуточный новый дескриптор, который вы научились создавать в предыдущем вопросе.

  - `10>&2` - новый дескриптор перенаправили в `stderr`
  - `2>&1` - `stderr` перенаправили в `stdout`
  - `1>&9` - `stdout` перенаправили в новый дескриптор

```bash
$ cat /test/test.txt 10>&2 2>&1 1>&10 
cat: /test/test.txt: Нет такого файла или каталога
$ cat /test/test.txt 10>&2 2>&1 1>&10 | wc -l
1
```

9. Что выведет команда `cat /proc/$$/environ`? Как еще можно получить аналогичный по содержанию вывод?

  - вывод переменные текущего процесса
  - аналогично `printenv` и `env`

10. Используя `man`, опишите что доступно по адресам `/proc/<PID>/cmdline`, `/proc/<PID>/exe`.

  - `/proc/<PID>/cmdline` - содержат параметры командной строки, переданные исполняемому файлу при запуске процесса `[PID]`
  - `/proc/<PID>/exe` - содержит ссылку до файла запущенного для процесса `[PID]`

```bash
$ echo $$
80691
$ cat /proc/80691/cmdline 
bash
$ ls -l /proc/80691/exe 
lrwxrwxrwx 1 aleksey aleksey 0 янв 16 20:03 /proc/80691/exe -> /usr/bin/bash
```
  - в соседнем терменале `man bash`
```bash
$ pgrep -fa man
83509 man bash
$ ls -l /proc/83509/exe 
lrwxrwxrwx 1 aleksey aleksey 0 янв 16 20:22 /proc/83509/exe -> /usr/bin/man
$ cat /proc/83509/cmdline 
manbash
```

11. Узнайте, какую наиболее старшую версию набора инструкций SSE поддерживает ваш процессор с помощью /proc/cpuinfo.

```bash
cat /proc/cpuinfo | grep -i sse
....sse4_2......
```

12. При открытии нового окна терминала и vagrant ssh создается новая сессия и выделяется pty. Это можно подтвердить командой tty, которая упоминалась в лекции 3.2.

  - Для удаленного сеанса по умолчанию не выделен TTY. Что-бы выделить TTY для удаленного выполнения (по man) необходимо использовать ключ -t.

13. Бывает, что есть необходимость переместить запущенный процесс из одной сессии в другую. Попробуйте сделать это, воспользовавшись reptyr. Например, так можно перенести в screen процесс, который вы запустили по ошибке в обычной SSH-сессии.

```bash
$ screen
$ top
ctrl+A+D
$ screen
$ screen -rd
There are several suitable screens on:
        88424.pts-16.mi-pro-15  (16.01.2022 20:52:06)   (Attached)
        88004.pts-16.mi-pro-15  (16.01.2022 20:49:43)   (Detached)
$ pgrep -a htop
88310 htop
$ reptyr 88310
Unable to attach to pid 88310: Operation not permitted
The kernel denied permission while attaching. If your uid matches
the target's, check the value of /proc/sys/kernel/yama/ptrace_scope.
For more information, see /etc/sysctl.d/10-ptrace.conf
$ echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
$ reptyr 88310
```

14. `sudo echo string > /root/new_file` не даст выполнить перенаправление под обычным пользователем, так как перенаправлением занимается процесс shell'а, который запущен без sudo под вашим пользователем. Для решения данной проблемы можно использовать конструкцию echo string | sudo tee /root/new_file. Узнайте что делает команда tee и почему в отличие от sudo echo команда с sudo tee будет работать.

  - tee - используется для записи вывода любой команды в один или несколько файлов.

*UPDATE:*
  - в данном случае shell, запущен от пользователя, выполняет команду `sudo echo..` выхлоп которой (`stdout`), в свою очередь, перенаправлен в файл, доступа к которому у пользователя нет.
```bash
vagrant@vagrant:~$ sudo echo string > /tmp/test/test1.txt
vagrant@vagrant:~$ echo string | sudo tee /tmp/test/test2.txt 
string
vagrant@vagrant:~$ ls -l /tmp/test/
-rw-rw-r--  1 vagrant vagrant    7 Jan 22 20:53 test1.txt
-rw-r--r--  1 root    root       7 Jan 22 20:54 test2.txt
```
  - чтобы это заработало, то необходимо запустить отдельный `shell` с правами `root`
```bash
$ sudo bash -c "echo string > /tmp/test/test3.txt"
vagrant@vagrant:~$ ls -l /tmp/test/
-rw-rw-r--  1 vagrant vagrant    7 Jan 22 20:53 test1.txt
-rw-r--r--  1 root    root       7 Jan 22 20:54 test2.txt
-rw-r--r--  1 root    root       7 Jan 22 21:09 test3.txt
```


