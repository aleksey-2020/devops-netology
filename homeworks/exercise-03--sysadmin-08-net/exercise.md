# Домашнее задание к занятию "3.8. Компьютерные сети, лекция 3"

1. Подключитесь к публичному маршрутизатору в интернет. Найдите маршрут к вашему публичному IP

    ```bash
    $ curl ifconfig.me
    185.119.0.160 

    $ telnet route-views.routeviews.org
    Trying 128.223.51.103...
    Connected to route-views.routeviews.org.
    ...
    User Access Verification

    route-views>show ip route 185.119.0.160
    Routing entry for 185.119.0.0/22
      Known via "bgp 6447", distance 20, metric 0
      Tag 6939, type external
      Last update from 64.71.137.241 2w6d ago
      Routing Descriptor Blocks:
      * 64.71.137.241, from 64.71.137.241, 2w6d ago
          Route metric is 0, traffic share count is 1
          AS Hops 2
          Route tag 6939
          MPLS label: none

    route-views>show bgp 185.119.0.160     
    BGP routing table entry for 185.119.0.0/22, version 2286573063
    Paths: (24 available, best #24, table default)
      Not advertised to any peer
      Refresh Epoch 1
      8283 31133 8492
        94.142.247.3 from 94.142.247.3 (94.142.247.3)
          Origin IGP, metric 0, localpref 100, valid, external
          Community: 8283:1 8283:101 8283:102 8492:1 8492:1500 8492:1501
          unknown transitive attribute: flag 0xE0 type 0x20 length 0x24
            value 0000 205B 0000 0000 0000 0001 0000 205B
                  0000 0005 0000 0001 0000 205B 0000 0005
                  0000 0002 
          path 7FE172C596A8 RPKI State valid
          rx pathid: 0, tx pathid: 0
      Refresh Epoch 1
      53767 174 31133 8492
        162.251.163.2 from 162.251.163.2 (162.251.162.3)
          Origin IGP, localpref 100, valid, external
          Community: 174:21101 174:22005 53767:5000
          path 7FE0A305E218 RPKI State valid
          rx pathid: 0, tx pathid: 0
    ....
    ```


2. Создайте dummy0 интерфейс в Ubuntu. Добавьте несколько статических маршрутов. Проверьте таблицу маршрутизации.

    ```bash
    $ sudo ip link add dummy0 type dummy
    $ ip a
    ....
    9: dummy0: <BROADCAST,NOARP> mtu 1500 qdisc noop state DOWN group default qlen 1000
        link/ether 46:c2:4c:fd:40:bd brd ff:ff:ff:ff:ff:ff
    $ sudo ip link delete dummy0
    $ sudo ip route add 192.168.55.0/24 via 192.168.52.1
    $ sudo ip route add 192.168.33.0/24 via 192.168.52.1
    $ ip route show
    default via 192.168.52.1 dev wlp3s0 proto static metric 600
    169.254.0.0/16 dev wlp3s0 scope link metric 1000
    172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 linkdown
    172.18.0.0/16 dev br-6a00a2d2fd06 proto kernel scope link src 172.18.0.1
    192.168.33.0/24 via 192.168.52.1 dev wlp3s0
    192.168.52.0/24 dev wlp3s0 proto kernel scope link src 192.168.52.19 metric 600
    192.168.55.0/24 via 192.168.52.1 dev wlp3s0
    $ sudo ip route delete 192.168.55.0/24
    $ sudo ip route delete 192.168.33.0/24
    ```

3. Проверьте открытые TCP порты в Ubuntu, какие протоколы и приложения используют эти порты? Приведите несколько примеров.

    - открытые порты
    ```bash
    $ sudo netstat -ntlp
    Активные соединения с интернетом (only servers)
    Proto Recv-Q Send-Q Local Address Foreign Address State       PID/Program name    
    tcp        0      0 127.0.0.1:63342         0.0.0.0:*               LISTEN      5616/java           
    tcp        0      0 127.0.0.1:5941          0.0.0.0:*               LISTEN      1881/teamviewerd    
    tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      1063/systemd-resolv 
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1275/sshd: /usr/sbi 
    tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1146/cupsd          
    tcp        0      0 127.0.0.1:6942          0.0.0.0:*               LISTEN      5616/java           
    tcp6       0      0 :::4369                 :::*                    LISTEN      1/init              
    tcp6       0      0 :::22                   :::*                    LISTEN      1275/sshd: /usr/sbi 
    tcp6       0      0 ::1:631                 :::*                    LISTEN      1146/cupsd   
    ```
   - на локальной машине открыт ssh демон с возможностью подключения со всех ipv4 & ipv6 \
     `tcp` - `0.0.0.0:22` \
     `tcp6` - `:::22`

   - открыт внутренний dns resolv на порту 53 на внутреннем хосте `127.0.0.53` \
     `$ dig A ya.ru @127.0.0.53 +short` \
     `87.250.250.242`

4. Проверьте используемые UDP сокеты в Ubuntu, какие протоколы и приложения используют эти порты?

    - Опция `-u` - показать только протокола UDP \
      Порты указаны в столбце `Local Address:Port` \
      Процессы (приложения) в столбце `Process`

    ```bash
    $ ss -nulp
    State  Recv-Q  Send-Q   Local Address:Port    Peer Address:Port Process                              
    UNCONN 0       0              0.0.0.0:631          0.0.0.0:*                                         
    UNCONN 0       0              0.0.0.0:58978        0.0.0.0:*                                         
    UNCONN 0       0              0.0.0.0:59156        0.0.0.0:*     users:(("firefox",pid=3851,fd=123)) 
    UNCONN 0       0              0.0.0.0:5353         0.0.0.0:*                                         
    UNCONN 0       0        127.0.0.53%lo:53           0.0.0.0:*                                         
    UNCONN 0       0                 [::]:60505           [::]:*                                         
    UNCONN 0       0                 [::]:5353            [::]:*  
    ```

5. Используя diagrams.net, создайте L3 диаграмму вашей домашней сети или любой другой сети, с которой вы работали.

    - router - 192.168.52.1
    - AP Wi-Fi - 192.168.52.2
    - ![step-01.png](screenshot/step-01.png?raw=true "step-01.png")
      

