# Домашнее задание к занятию "3.6. Компьютерные сети, лекция 1"

1. Работа c HTTP через телнет. 
  - Подключитесь утилитой телнет к сайту stackoverflow.com telnet stackoverflow.com 80 
  - отправьте HTTP запрос

    ```bash
    GET /questions HTTP/1.0
    HOST: stackoverflow.com
    [press enter]
    [press enter]
    ```
  - В ответе укажите полученный HTTP код, что он означает?

```bash
$ telnet stackoverflow.com 80
Trying 151.101.65.69...
Connected to stackoverflow.com.
Escape character is '^]'.
GET /questions HTTP/1.0
HOST: stackoverflow.com

HTTP/1.1 301 Moved Permanently
cache-control: no-cache, no-store, must-revalidate
location: https://stackoverflow.com/questions
x-request-guid: b77bb1ca-63de-4ac9-a16f-ee5277942ba8
feature-policy: microphone 'none'; speaker 'none'
content-security-policy: upgrade-insecure-requests; frame-ancestors 'self' https://stackexchange.com
Accept-Ranges: bytes
Date: Sat, 12 Feb 2022 15:01:25 GMT
Via: 1.1 varnish
Connection: close
X-Served-By: cache-hhn4059-HHN
X-Cache: MISS
X-Cache-Hits: 0
X-Timer: S1644678086.700599,VS0,VE155
Vary: Fastly-SSL
X-DNS-Prefetch-Control: off
Set-Cookie: prov=72203564-72b7-6f08-f2a5-3cedbedaf5a2; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly

Connection closed by foreign host.

```
  - при запросе получаем направление куда идти, постоянный редирект (301): \
    `http://stackoverflow.com/questions` --> `https://stackoverflow.com/questions`

2. Повторите задание 1 в браузере, используя консоль разработчика F12.

  - откройте вкладку Network
  - отправьте запрос http://stackoverflow.com
  - найдите первый ответ HTTP сервера, откройте вкладку Headers
  - укажите в ответе полученный HTTP код.:
    - ![step-01.png](screenshot/step-01.png?raw=true "step-01.png")
  - проверьте время загрузки страницы, какой запрос обрабатывался дольше всего?
    - время загрузки страницы `1.4сек`
    - ожидание и получение страницы `/questions`
    - ![step-02.png](screenshot/step-02.png?raw=true "step-02.png")
  - приложите скриншот консоли браузера в ответ.

3. Какой IP адрес у вас в интернете?

```bash
$ curl ifconfig.me
185.119.0.32
```

4.Какому провайдеру принадлежит ваш IP адрес? Какой автономной системе AS? Воспользуйтесь утилитой whois

```bash
$ whois 185.119.0.32 | grep -e descr -e netname -e AS
netname:        RU-OBIT-20150928
descr:          Obit-Telecommunications Ltd.
descr:          St.Petersburg
origin:         AS8492
```

5. Через какие сети проходит пакет, отправленный с вашего компьютера на адрес 8.8.8.8? Через какие AS? Воспользуйтесь утилитой traceroute

```bash
$ traceroute -An 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  * * *
 2  * * *
 3  172.29.255.0 [*]  5.466 ms  5.407 ms  9.050 ms
 4  * * *
 5  * * *
 6  172.29.194.105 [*]  10.745 ms  7.245 ms  7.140 ms
 7  85.114.1.12 [AS8492]  5.406 ms  3.279 ms  5.449 ms
 8  72.14.198.236 [AS15169]  5.360 ms  4.735 ms  6.539 ms
 9  74.125.244.132 [AS15169]  2.240 ms  2.714 ms  3.040 ms
10  216.239.48.163 [AS15169]  5.664 ms 142.251.61.219 [AS15169]  6.402 ms 72.14.232.85 [AS15169]  2.714 ms
11  74.125.253.147 [AS15169]  7.042 ms * 142.251.61.221 [AS15169]  6.672 ms
12  * * *
13  * * *
14  * * *
15  * * *
16  * * *
17  * * *
18  * * *
19  * * *
20  * * *
21  * 8.8.8.8 [AS15169]  7.666 ms  7.304 ms
```

6. Повторите задание 5 в утилите mtr. На каком участке наибольшая задержка - delay?
    - ![step-03.png](screenshot/step-03.png?raw=true "step-03.png")
    - `72.14.198.236 [AS15169]`

7. Какие DNS сервера отвечают за доменное имя dns.google? Какие A записи? воспользуйтесь утилитой dig

```bash
$ dig A dns.google

; <<>> DiG 9.16.1-Ubuntu <<>> A dns.google
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47075
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;dns.google.			IN	A

;; ANSWER SECTION:
dns.google.		746	IN	A	8.8.8.8
dns.google.		746	IN	A	8.8.4.4

;; Query time: 8 msec
;; SERVER: 172.18.0.2#53(172.18.0.2)
;; WHEN: Сб фев 12 19:59:03 MSK 2022
;; MSG SIZE  rcvd: 71

```

```bash
$ dig NS dns.google

; <<>> DiG 9.16.1-Ubuntu <<>> NS dns.google
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37614
;; flags: qr rd ra; QUERY: 1, ANSWER: 4, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;dns.google.			IN	NS

;; ANSWER SECTION:
dns.google.		21600	IN	NS	ns4.zdns.google.
dns.google.		21600	IN	NS	ns3.zdns.google.
dns.google.		21600	IN	NS	ns1.zdns.google.
dns.google.		21600	IN	NS	ns2.zdns.google.

;; Query time: 12 msec
;; SERVER: 172.18.0.2#53(172.18.0.2)
;; WHEN: Сб фев 12 19:54:49 MSK 2022
;; MSG SIZE  rcvd: 116

```

8. Проверьте PTR записи для IP адресов из задания 7. Какое доменное имя привязано к IP? воспользуйтесь утилитой dig

```bash
$ dig -x 8.8.8.8

; <<>> DiG 9.16.1-Ubuntu <<>> -x 8.8.8.8
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3901
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;8.8.8.8.in-addr.arpa.		IN	PTR

;; ANSWER SECTION:
8.8.8.8.in-addr.arpa.	72465	IN	PTR	dns.google.

;; Query time: 4 msec
;; SERVER: 172.18.0.2#53(172.18.0.2)
;; WHEN: Сб фев 12 20:00:53 MSK 2022
;; MSG SIZE  rcvd: 73

```

