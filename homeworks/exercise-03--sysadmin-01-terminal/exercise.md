
# Домашнее задание по лекции "Работа в терминале (лекция 1)"


1. Установите средство виртуализации Oracle VirtualBox.

```bash
$ virtualbox --help
Oracle VM VirtualBox VM Selector v6.1.26_Ubuntu
(C) 2005-2021 Oracle Corporation
All rights reserved.

No special options.
```

2. Установите средство автоматизации Hashicorp Vagrant.

```bash
$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
$ sudo apt-get update && sudo apt-get install vagrant
```

```
$ vagrant -v
Vagrant 2.2.19
```

3. В вашем основном окружении подготовьте удобный для дальнейшей работы терминал

```done```

4. С помощью базового файла конфигурации запустите Ubuntu 20.04 в VirtualBox посредством Vagrant

[done](homeworks/exercise-03--sysadmin-01-terminal/Vagrant/Vagrantfile "done")

```bash
$ vagrant status
Current machine states:

default                   running (virtualbox)

```

5. Ознакомьтесь с графическим интерфейсом VirtualBox. Какие ресурсы выделены по-умолчанию?

```bash
$ VBoxManage showvminfo Vagrant_default_1642335030066_57555 | grep -e "Name:[ ]*V" -e "Memory size:" -e "Number of CPUs:" -e "Guest OS:"
Name:                        Vagrant_default_1642335030066_57555
Guest OS:                    Ubuntu (64-bit)
Memory size:                 1024MB
Number of CPUs:              2
```

6. Ознакомьтесь с возможностями конфигурации VirtualBox через Vagrantfile: документация. Как добавить оперативной памяти или ресурсов процессора виртуальной машине?

[done](homeworks/exercise-03--sysadmin-01-terminal/Vagrant/Vagrantfile "done")

7. Команда vagrant ssh...

```bash
$ vagrant ssh default
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
...

vagrant@vagrant:~$ 
```

8. Ознакомиться с разделами man bash
   
```done```

9. В каких сценариях использования применимы скобки {} и на какой строчке man bash это описано?

```bash
$ man bash | grep -n '{ list; }'
181:       { list; }
```

10. С учётом ответа на предыдущий вопрос, как создать однократным вызовом touch 100000 файлов? Получится ли аналогичным образом создать 300000? Если нет, то почему?

```bash
$ touch /tmp/test/test-{000001..100000}.txt
```

"300000" - слишком дилинный список аргументов

```bash
$ touch /tmp/test/test-{000001..300000}.txt
-bash: /usr/bin/touch: Argument list too long

$ getconf -a | grep ARG_MAX
ARG_MAX                            2097152
_POSIX_ARG_MAX                     2097152
```
11. В man bash поищите по /\[\[. Что делает конструкция `[[ -d /tmp ]]`

 - проверяет наличие фпйлов
 - особенности использования `[[` в операторе `if` [тут](https://github.com/hightemp/docLinux/blob/master/articles/%D0%A3%D1%81%D0%BB%D0%BE%D0%B2%D0%B8%D1%8F%20%D0%B2%20%D1%81%D0%BA%D1%80%D0%B8%D0%BF%D1%82%D0%B0%D1%85%20bash%20(%D1%83%D1%81%D0%BB%D0%BE%D0%B2%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%82%D0%BE%D1%80%D1%8B).md#2-%D1%81%D0%B8%D0%BD%D1%82%D0%B0%D0%BA%D1%81%D0%B8%D1%81-%D0%B2-%D0%B4%D0%B2%D0%BE%D0%B9%D0%BD%D1%8B%D1%85-%D1%81%D0%BA%D0%BE%D0%B1%D0%BA%D0%B0%D1%85)

12. Основываясь на знаниях о просмотре текущих (например, PATH) и установке новых переменных; командах, которые мы рассматривали, добейтесь в выводе type -a bash в виртуальной машине наличия первым пунктом в списке:

```bash
$ mkdir /tmp/test/
$ cp /bin/bash /tmp/test/
$ type -a bash
bash is /usr/bin/bash
bash is /bin/bash

$ PATH=/tmp/test/:$PATH
$ type -a bash
bash is /tmp/test/bash
bash is /usr/bin/bash
bash is /bin/bash
```

13. Чем отличается планирование команд с помощью batch и at?

 - `at`, `batch`, `atq`, `atrm` — ставить в очередь, проверять или удалять задания для последующего выполнения
 - Утилита `at` считывает команду из стандартного ввода и выполняет их в более позднее время. В отличие от `crontab`, задания, созданные с помощью `at`, выполняются только один раз.
 - `batch` - выполняет команды, когда позволяют уровни загрузки системы; другими словами, когда среднее значение нагрузки падает ниже `0,8` или значения, указанного при вызове `atrun`

14. Завершите работу виртуальной машины чтобы не расходовать ресурсы компьютера и/или батарею ноутбука.

```
$ vagrant halt
==> default: Attempting graceful shutdown of VM...
```
