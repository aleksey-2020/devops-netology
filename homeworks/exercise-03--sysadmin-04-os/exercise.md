
# Домашнее задание к занятию "3.4. Операционные системы, лекция 2"

1. На лекции мы познакомились с node_exporter. ..... создайте самостоятельно простой unit-файл для node_exporter:

```bash
root@vagrant:# systemctl cat node_exporter
# /etc/systemd/system/node_exporter.service
[Unit]
Description=node_exporter
After=network.target

[Service]
ExecStart=/usr/local/bin/node_exporter
Restart=Always

[Install]
WantedBy=multi-user.target


# systemctl status node_exporter
● node_exporter.service - node_exporter
     Loaded: loaded (/etc/systemd/system/node_exporter.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2022-01-22 18:12:54 UTC; 3min 6s ago
   Main PID: 1544 (node_exporter)
      Tasks: 4 (limit: 1071)
     Memory: 2.3M
     CGroup: /system.slice/node_exporter.service
             └─1544 /usr/local/bin/node_exporter

```

2. Ознакомьтесь с опциями `node_exporter` и выводом `/metrics` по-умолчанию. Приведите несколько опций, которые вы бы выбрали для базового мониторинга хоста по CPU, памяти, диску и сети.

```bash
# curl -s 127.0.0.1:9100/metrics | grep -i -e "node_cpu_seconds_total" -e "node_memory_MemFree_bytes" -e "sda" -e "eth0"

node_arp_entries{device="eth0"} 2
node_cpu_seconds_total{cpu="0",mode="idle"} 3910.85
node_cpu_seconds_total{cpu="0",mode="iowait"} 1.53
node_cpu_seconds_total{cpu="0",mode="irq"} 0
node_cpu_seconds_total{cpu="0",mode="nice"} 0.01
node_cpu_seconds_total{cpu="0",mode="softirq"} 0.35
node_cpu_seconds_total{cpu="0",mode="steal"} 0
node_cpu_seconds_total{cpu="0",mode="system"} 5.47
node_cpu_seconds_total{cpu="0",mode="user"} 2.84
node_disk_discard_time_seconds_total{device="sda"} 0
node_disk_discarded_sectors_total{device="sda"} 0
node_disk_discards_completed_total{device="sda"} 0
node_disk_discards_merged_total{device="sda"} 0
node_disk_info{device="sda",major="8",minor="0"} 1
node_disk_io_now{device="sda"} 0
node_disk_io_time_seconds_total{device="sda"} 5.968
node_disk_io_time_weighted_seconds_total{device="sda"} 1.772
node_disk_read_bytes_total{device="sda"} 2.76269056e+08
node_disk_read_time_seconds_total{device="sda"} 2.981
node_disk_reads_completed_total{device="sda"} 6708
node_disk_reads_merged_total{device="sda"} 2706
node_disk_write_time_seconds_total{device="sda"} 3.247
node_disk_writes_completed_total{device="sda"} 2788
node_disk_writes_merged_total{device="sda"} 2862
node_disk_written_bytes_total{device="sda"} 7.70048e+07
node_filesystem_avail_bytes{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 8.41621504e+08
node_filesystem_device_error{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 0
node_filesystem_files{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 65536
node_filesystem_files_free{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 65224
node_filesystem_free_bytes{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 9.12084992e+08
node_filesystem_readonly{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 0
node_filesystem_size_bytes{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 1.02330368e+09
node_memory_MemFree_bytes 4.30956544e+08
node_network_address_assign_type{device="eth0"} 0
node_network_carrier{device="eth0"} 1
node_network_carrier_changes_total{device="eth0"} 2
node_network_carrier_down_changes_total{device="eth0"} 1
node_network_carrier_up_changes_total{device="eth0"} 1
node_network_device_id{device="eth0"} 0
node_network_dormant{device="eth0"} 0
node_network_flags{device="eth0"} 4099
node_network_iface_id{device="eth0"} 2
node_network_iface_link{device="eth0"} 2
node_network_iface_link_mode{device="eth0"} 0
node_network_info{address="08:00:27:b1:28:5d",broadcast="ff:ff:ff:ff:ff:ff",device="eth0",duplex="full",ifalias="",operstate="up"} 1
node_network_mtu_bytes{device="eth0"} 1500
node_network_net_dev_group{device="eth0"} 0
node_network_protocol_type{device="eth0"} 1
node_network_receive_bytes_total{device="eth0"} 1.0865729e+07
node_network_receive_compressed_total{device="eth0"} 0
node_network_receive_drop_total{device="eth0"} 0
node_network_receive_errs_total{device="eth0"} 0
node_network_receive_fifo_total{device="eth0"} 0
node_network_receive_frame_total{device="eth0"} 0
node_network_receive_multicast_total{device="eth0"} 0
node_network_receive_packets_total{device="eth0"} 13626
node_network_speed_bytes{device="eth0"} 1.25e+08
node_network_transmit_bytes_total{device="eth0"} 519885
node_network_transmit_carrier_total{device="eth0"} 0
node_network_transmit_colls_total{device="eth0"} 0
node_network_transmit_compressed_total{device="eth0"} 0
node_network_transmit_drop_total{device="eth0"} 0
node_network_transmit_errs_total{device="eth0"} 0
node_network_transmit_fifo_total{device="eth0"} 0
node_network_transmit_packets_total{device="eth0"} 5401
node_network_transmit_queue_length{device="eth0"} 1000
node_network_up{device="eth0"} 1

```

3. Установите в свою виртуальную машину Netdata. Воспользуйтесь готовыми пакетами для установки (sudo apt install -y netdata)...

![Netdata](screenshot/step-01.png?raw=true "Netdata")

4. Можно ли по выводу dmesg понять, осознает ли ОС, что загружена не на настоящем оборудовании, а на системе виртуализации?

- ответ: да
    - в виртуальной машине
    ```bash
    $ dmesg | grep virtualiz
    [    0.001563] CPU MTRRs all blank - virtualized system.
    [    0.067297] Booting paravirtualized kernel on KVM
    [    2.555946] systemd[1]: Detected virtualization oracle.
    ```
    - на хостовой машине
   ```bash
    $ dmesg | grep virtualiz
    [    0.111425] Booting paravirtualized kernel on bare hardware
    ```
5. Как настроен sysctl fs.nr_open на системе по-умолчанию? Узнайте, что означает этот параметр. Какой другой существующий лимит не позволит достичь такого числа (ulimit --help)?

- хорошо описано [тут 1](https://ru.stackoverflow.com/questions/475417/%D0%9B%D0%B8%D0%BC%D0%B8%D1%82-%D0%BD%D0%B0-%D0%BA%D0%BE%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%BE-%D0%BE%D1%82%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D1%85-%D0%B4%D0%B5%D1%81%D0%BA%D1%80%D0%B8%D0%BF%D1%82%D0%BE%D1%80%D0%BE%D0%B2) и [тут 2](https://cyber-x.ru/%D0%BA%D0%B0%D0%BA-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B8%D1%82%D1%8C-%D0%BE%D0%B3%D1%80%D0%B0%D0%BD%D0%B8%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BD%D0%B0-%D0%BE%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%B8%D0%B5/) 
- текущее ограничение:
```bash
$ /sbin/sysctl -n fs.nr_open
1048576
```
- `nr_open` - жесткий лимит на открытые дескрипторы, более полезен параметр `file-max` - мягкий лимит.

- `жесткий лимит` может изменить только от пользователя на уровне системы
- `мягкий лимит` может изменить процесс или пользователь, и только в меньшую сторону

6. Запустите любой долгоживущий процесс (не ls, который отработает мгновенно, а, например, sleep 1h) в отдельном неймспейсе процессов; покажите, что ваш процесс работает под PID 1 через nsenter. Для простоты работайте в данном задании под root (sudo -i). Под обычным пользователем требуются дополнительные опции (--map-root-user) и т.д.

- консоль 1
```bash
# unshare -f --pid --mount-proc sleep 1h
```
- консоль 2
```bash
$ pgrep -a sleep
1656 sleep 1h
$ nsenter -t 1656 -p -m
nsenter: cannot open /proc/1656/ns/pid: Permission denied

$ sudo nsenter -t 1656 -p -m

# ps -af
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 19:43 pts/1    00:00:00 sleep 1h
root           2       0  0 19:46 pts/0    00:00:00 -bash
root          23       2  0 19:48 pts/0    00:00:00 ps -af
```

7. Найдите информацию о том, что такое `:(){ :|:& };:`.

- прикольный `fork bomb`, бесконечно создающая сам себя, забивая свободное место в списке активных процессов.

```bash
$ :(){ :|:& };:
.....
-bash: fork: Resource temporarily unavailable
-bash: fork: retry: Resource temporarily unavailable
-bash: fork: retry: Resource temporarily unavailable
-bash: fork: Resource temporarily unavailable
.....

$ ps -af
UID          PID    PPID  C STIME TTY          TIME CMD
vagrant    36112       1  0 20:07 pts/0    00:00:00 -bash
vagrant    36225       1  0 20:07 pts/0    00:00:00 -bash
vagrant    36303       1  0 20:07 pts/0    00:00:00 -bash
....

$ dmesg
....
[ 2929.547286] cgroup: fork rejected by pids controller in /user.slice/user-1000.slice/session-3.scope

```

- можно ограничить по количеству запускаемых процессов для пользователя `ulimit -u`
```bash
$ ulimit -u
3571
$ ulimit -u 20
$ ulimit -u
20
```
